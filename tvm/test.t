function main
    vars
     i 1
     c 1
     a 10
    endvars

    %5 = 3
    readi %6
    
    %7 = &a
    %7 = %7 + %5
    
    *%7 = %6

    %10 = 10
    i = 0
    c = ','

    label for1 :
    %1 = i < %10
    ifFalse %1 goto endfor1

    %2 = a[i]
    writei %2
    writec c

    %3 = 1
    i = i + %3
    goto for1

    label endfor1 :

    return
endfunction
