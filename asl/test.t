function test
  params
    _a
  endparams
  vars
    a 1
  endvars
    


    writei a
    writeln

    %4 = &a
    writei %4
    writeln

  return

endfunction

function main
  vars
    b 20
    a 10
  endvars
    
    %20 = 0
    %3 = 3
    %1 = 1
    a[%1] = %3
    a[%20] = %3

    %4 = &a
    writei %4
    writeln
    pushparam %4
    call test

    return
endfunction


