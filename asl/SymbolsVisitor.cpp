//////////////////////////////////////////////////////////////////////
//
//    SymbolsVisitor - Walk the parser tree to register symbols
//                     for the Asl programming language
//
//    Copyright (C) 2019  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.  SPAIN
//
//////////////////////////////////////////////////////////////////////

#include "SymbolsVisitor.h"

#include "antlr4-runtime.h"

#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/SemErrors.h"

#include <iostream>
#include <string>
#include <vector>

#include <cstddef>    // std::size_t

// uncomment the following line to enable debugging messages with DEBUG*
// #define DEBUG_BUILD
#include "../common/debug.h"

// using namespace std;


// Constructor
SymbolsVisitor::SymbolsVisitor(TypesMgr       & Types,
			       SymTable       & Symbols,
			       TreeDecoration & Decorations,
			       SemErrors      & Errors) :
  Types{Types},
  Symbols{Symbols},
  Decorations{Decorations},
  Errors{Errors} {
}


// Methods to visit each kind of node:

#define c(n) AslParser::n##Context*
#define visitd(n) antlrcpp::Any SymbolsVisitor::visit##n(c(n) ctx)

visitd(Program) {
  DEBUG_ENTER();
  
  SymTable::ScopeId sc = Symbols.pushNewScope("$global$");
  putScopeDecor(ctx, sc);

  for (c(Function) fnc : ctx->function()) {
    visit(fnc);
  }
  
  Symbols.popScope();
  DEBUG_EXIT();
  return 0;
}

visitd(Function) {
  DEBUG_ENTER();
  
  std::string funcName = ctx->ID()->getText();
  SymTable::ScopeId sc = Symbols.pushNewScope(funcName);
  putScopeDecor(ctx, sc);
    
  std::vector<TypesMgr::TypeId> argsTy;
  if (ctx->arglist()) {
    std::vector<TypesMgr::TypeId> args = visit(ctx->arglist());
    argsTy = args;
  }

  TypesMgr::TypeId retTy = Types.createVoidTy();
  if (ctx->type()) {
    TypesMgr::TypeId ret = visit(ctx->type());
    retTy = ret;
  }

  for (AslParser::DeclarationContext* dcl : ctx->declaration())
    visit(dcl);

  Symbols.popScope();
  TypesMgr::TypeId funcTy = Types.createFunctionTy(argsTy, retTy);
  putTypeDecor(ctx, funcTy);

  
  if (Symbols.findInCurrentScope(funcName))
    Errors.declaredIdent(ctx->ID());
  else
    Symbols.addFunction(funcName, funcTy);
    
  DEBUG_EXIT();
  return 0;
}

visitd(Declaration) {
  TypesMgr::TypeId type = visit(ctx->type());
  putTypeDecor(ctx, type);

  for (auto idp : ctx->ID()) {
    std::string id = idp->getText();

    if (Symbols.findInCurrentScope(id))
      Errors.declaredIdent(idp);
    else
      Symbols.addLocalVar(id, type);
  }
  return 0;
}

visitd(Arglist) {
  std::vector<TypesMgr::TypeId> types(ctx->arg().size());
  int i = 0;
  for (c(Arg) a : ctx->arg()) {
    std::string name = a->ID()->getText();
    TypesMgr::TypeId type = visit(a->type());

    if (Symbols.findInCurrentScope(name))
      Errors.declaredIdent(a->ID());
    else
      Symbols.addParameter(name, type);
    
    types[i++] = type;
  }
  return types;
}

visitd(Type) {
  TypesMgr::TypeId basic = visit(ctx->basictype());
  if (ctx->ARRAY())
    // NOTE(ernesto): stoi is safe because the parser checks it.
    return Types.createArrayTy(std::stoi(ctx->INTLIT()->getText()), basic);
  return basic;
}

visitd(Basictype) {
  if (ctx->INT())
    return Types.createIntegerTy();
  if (ctx->FLOAT())
    return Types.createFloatTy();
  if (ctx->BOOL())
    return Types.createBooleanTy();
  if (ctx->CHAR())
    return Types.createCharacterTy();
  return Types.createErrorTy();
}

#undef c
#undef visitd

// Getters for the necessary tree node atributes:
//   Scope and Type
SymTable::ScopeId SymbolsVisitor::getScopeDecor(antlr4::ParserRuleContext *ctx) {
  return Decorations.getScope(ctx);
}
TypesMgr::TypeId SymbolsVisitor::getTypeDecor(antlr4::ParserRuleContext *ctx) {
  return Decorations.getType(ctx);
}

// Setters for the necessary tree node attributes:
//   Scope and Type
void SymbolsVisitor::putScopeDecor(antlr4::ParserRuleContext *ctx, SymTable::ScopeId s) {
  Decorations.putScope(ctx, s);
}
void SymbolsVisitor::putTypeDecor(antlr4::ParserRuleContext *ctx, TypesMgr::TypeId t) {
  Decorations.putType(ctx, t);
}
