#!/bin/bash

fname="../examples/jp_genc_$1.asl"
if [ "$2" == "view" ]; then
    cat -n $fname
    echo ""
elif [ "$2" == "run" ]
then
    echo $(basename "$fname")
     ./asl "$fname" > tmp.t
     ../tvm/tvm tmp.t < "${fname/asl/in}" > tmp.out
     echo "yours:"
     cat tmp.out
     echo "his:"
     cat ${fname/asl/out}
     echo "diff:"
     diff tmp.out "${fname/asl/out}"

else
    echo $(basename $fname)
    echo "yours:"
    ./asl $fname | egrep -v '^\(' > tmp.t
    cat tmp.t
    echo "his:"
    cat ${fname/asl/t}
    echo "diff:"
    diff tmp.t ${fname/asl/t}
    rm -f tmp.t
fi
