//////////////////////////////////////////////////////////////////////
//
//    TypeCheckVisitor - Walk the parser tree to do the semantic
//                       typecheck for the Asl programming language
//
//    Copyright (C) 2019  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.  SPAIN
//
//////////////////////////////////////////////////////////////////////


#include "TypeCheckVisitor.h"

#include "antlr4-runtime.h"

#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/SemErrors.h"

#include <iostream>
#include <string>

// uncomment the following line to enable debugging messages with DEBUG*
// #define DEBUG_BUILD
#include "../common/debug.h"

// using namespace std;


// Constructor
TypeCheckVisitor::TypeCheckVisitor(TypesMgr       & Types,
				   SymTable       & Symbols,
				   TreeDecoration & Decorations,
				   SemErrors      & Errors) :
  Types{Types},
  Symbols {Symbols},
  Decorations{Decorations},
  Errors{Errors} {
}

// Methods to visit each kind of node:
#define c(n) AslParser::n##Context*
#define visitd(n) antlrcpp::Any TypeCheckVisitor::visit##n(c(n) ctx)

visitd(Program) {
  SymTable::ScopeId sc = getScopeDecor(ctx);
  Symbols.pushThisScope(sc);  

  for (c(Function) func : ctx->function())
    visit(func);
    
  if (Symbols.noMainProperlyDeclared())
    Errors.noMainProperlyDeclared(ctx);

  Symbols.popScope();

  Errors.print();
  return 0;
}

visitd(Function) {
  TypesMgr::TypeId funcTy = getTypeDecor(ctx);
  SymTable::ScopeId sc = getScopeDecor(ctx);
  Symbols.pushThisScope(sc);
  Symbols.setCurrentFunctionTy(funcTy);
  
  for (c(Statement) st : ctx->statements()->statement())
    visit(st);
  
  Symbols.popScope();
  Symbols.setCurrentFunctionTy(Types.createErrorTy());
  return 0;
}

// Statement Rule

visitd(If) {
  TypesMgr::TypeId condTy = visit(ctx->expression());
  if (!Types.isBooleanTy(condTy) && !Types.isErrorTy(condTy))
    Errors.booleanRequired(ctx);

  // Visit IF branch
  for (c(Statement) st : ctx->statements()[0]->statement())
    visit(st);
  // Visit ELSE branch if exist
  if (ctx->statements().size() > 1)
    for (c(Statement) st : ctx->statements()[1]->statement())
      visit(st);

  return 0;
}

visitd(While) {
  TypesMgr::TypeId condTy = visit(ctx->expression());
  if (!Types.isBooleanTy(condTy) && !Types.isErrorTy(condTy))
    Errors.booleanRequired(ctx);

  for (c(Statement) st : ctx->statements()->statement())
    visit(st);

  return 0;
}

visitd(Return) {
  TypesMgr::TypeId retTy = ctx->expression() ?
    (TypesMgr::TypeId)visit(ctx->expression()) : Types.createVoidTy();
  
  if (!Types.copyableTypes(Types.getFuncReturnType(Symbols.getCurrentFunctionTy()), retTy) &&
      !Types.isErrorTy(retTy))
    Errors.incompatibleReturn(ctx->RETURN());
  
  return 0;
}

visitd(Read) {
  TypesMgr::TypeId rTy = visit(ctx->assignable());
  putTypeDecor(ctx, rTy);

  if (Types.isFunctionTy(rTy)) {
    Errors.nonReferenceableExpression(ctx);
    return 0;
  }
  
  if (!Types.isPrimitiveNonVoidTy(rTy) && !Types.isErrorTy(rTy))
    Errors.readWriteRequireBasic(ctx);
  return 0;
}

visitd(Write) {
  if (ctx->expression()) {
    TypesMgr::TypeId wTy = visit(ctx->expression());
    putTypeDecor(ctx, wTy);
    if (!Types.isPrimitiveNonVoidTy(wTy) && !Types.isErrorTy(wTy))
      Errors.readWriteRequireBasic(ctx);
  }
  return 0;
}

visitd(Assign) {
  TypesMgr::TypeId assTy = visit(ctx->assignable());
  TypesMgr::TypeId expTy = visit(ctx->expression());

  if (Types.isErrorTy(assTy) || Types.isErrorTy(expTy))
    return 0;

  if (Types.isFunctionTy(assTy))
    Errors.nonReferenceableLeftExpr(ctx->assignable());

  if (!Types.copyableTypes(assTy, expTy))
    Errors.incompatibleAssignment(ctx->ASSIGN());

  return 0;
}

/*
visitd(Pcall) {
  std::vector<c(Expression)> exp = ctx->expression();
  TypesMgr::TypeId funcTy = visit(exp[0]);

  if (Types.isErrorTy(funcTy))
    return 0;

  if (!Types.isFunctionTy(funcTy)) {
    Errors.isNotCallable(exp[0]);
    return 0;
  }
  
  std::vector<TypesMgr::TypeId> parsTy = Types.getFuncParamsTypes(funcTy);
  if (exp.size() - 1 != parsTy.size())
    Errors.numberOfParameters(exp[0]);

  for (int i = 1; i < exp.size(); i++) {
    TypesMgr::TypeId expTy = visit(exp[i]);
    if (i-1 < parsTy.size() && !Types.copyableTypes(parsTy[i-1], expTy)
        && !Types.isErrorTy(expTy))
      Errors.incompatibleParameter(exp[i], i, ctx);
  }
  
  return 0;
}
*/

visitd(Expr) {
  visit(ctx->expression());
  return 0;
}

// Expression rule

visitd(Lit) {
  TypesMgr::TypeId ty = visit(ctx->literal());
  putTypeDecor(ctx, ty);
  return ty;
}

visitd(Ident) {
  TypesMgr::TypeId idTy = Symbols.getType(ctx->ID()->getText());
  putTypeDecor(ctx, idTy);

  if (Types.isErrorTy(idTy))
    Errors.undeclaredIdent(ctx->ID());

  return idTy;
}

visitd(Parexpr) {
  TypesMgr::TypeId ty = visit(ctx->expression());
  putTypeDecor(ctx, ty);
  return ty;
}

visitd(Fcall) {
  std::vector<c(Expression)> exp = ctx->expression();

  if (Symbols.findInStack(ctx->ID()->getText()) == -1) {
      Errors.undeclaredIdent(ctx->ID());
      return Types.createErrorTy();
  }
  TypesMgr::TypeId funcTy = Symbols.getType(ctx->ID()->getText());
  
  if (!Types.isFunctionTy(funcTy)) {
    Errors.isNotCallable(ctx);
    return Types.createErrorTy();
  }
  
  std::vector<TypesMgr::TypeId> parsTy = Types.getFuncParamsTypes(funcTy);
  if (exp.size() != parsTy.size())
    Errors.numberOfParameters(ctx);
 
  for (unsigned int i = 0; i < exp.size(); i++) {
    TypesMgr::TypeId expTy = visit(exp[i]);
    putTypeDecor(exp[i], expTy);
    if (i < parsTy.size() && !Types.copyableTypes(parsTy[i], expTy) &&
        !Types.isErrorTy(expTy))
      Errors.incompatibleParameter(exp[i], i+1, ctx);
  }

  if (Types.isVoidFunction(funcTy) &&
      (dynamic_cast<c(Expression)>(ctx->parent) != nullptr ||
       dynamic_cast<c(Assign)>(ctx->parent) != nullptr)) {
    Errors.isNotFunction(ctx);
    return Types.createErrorTy();
  }
  
  TypesMgr::TypeId ty = Types.getFuncReturnType(funcTy);
  putTypeDecor(ctx, ty);
  return ty;
}

visitd(Arrayindex) {
  std::string id = ctx->ID()->getText();
  
  if (Symbols.findInStack(id) == -1) {
    Errors.undeclaredIdent(ctx->ID());
    return Types.createErrorTy();
  }

  TypesMgr::TypeId arrTy = Symbols.getType(id);

  TypesMgr::TypeId iTy = visit(ctx->expression());
  if (Types.isErrorTy(iTy))
    return iTy;
  
  if (!Types.isIntegerTy(iTy))
    Errors.nonIntegerIndexInArrayAccess(ctx->expression());

  if (Types.isErrorTy(arrTy))
    return arrTy;
  
  if (!Types.isArrayTy(arrTy)) {
    Errors.nonArrayInArrayAccess(ctx);
    return Types.createErrorTy();
  }
  
  TypesMgr::TypeId retTy = Types.getArrayElemType(arrTy);
  putTypeDecor(ctx, retTy);
  return retTy;
}

visitd(Unop) {
  TypesMgr::TypeId type = visit(ctx->expression());
  putTypeDecor(ctx, type);

  auto op = ctx->op->getType();

  if (op == AslParser::NOT) {
    if (!Types.isBooleanTy(type) && !Types.isErrorTy(type)) {
      Errors.booleanRequired(ctx);
      return Types.createErrorTy();
    }
    return type;
  }

  if (op == AslParser::PLUS || op == AslParser::MINUS) {
    if (!Types.isNumericTy(type) && !Types.isErrorTy(type)) {
      Errors.incompatibleOperator(ctx->op);
      return Types.createErrorTy();
    }
    return type;
  }

  // This will produce a Bad Cast error but in theory this line
  // is never executed
  return 0;
}

visitd(Binop) {
  TypesMgr::TypeId typel = visit(ctx->expression()[0]);
  TypesMgr::TypeId typer = visit(ctx->expression()[1]);
  auto op = ctx->op->getType();

  if (Types.isErrorTy(typel)) return typel;
  if (Types.isErrorTy(typer)) return typer;

  if (op == AslParser::MOD) {
    if (!Types.isIntegerTy(typel) || !Types.isIntegerTy(typer)) {
      Errors.incompatibleOperator(ctx->op);
      return Types.createIntegerTy();
    }
    putTypeDecor(ctx, typel);
    return typel;
  }

  // Arithmetic operator
  if (op == AslParser::MUL || op == AslParser::DIV ||
      op == AslParser::PLUS || op == AslParser::MINUS) {
    if (!Types.isNumericTy(typel) || !Types.isNumericTy(typer)) {
      Errors.incompatibleOperator(ctx->op);
      return Types.createIntegerTy();
    }
    if (Types.isFloatTy(typel) || Types.isFloatTy(typer)) {
      TypesMgr::TypeId fty = Types.createFloatTy();

      putTypeDecor(ctx, fty);
      return fty;
    }

    putTypeDecor(ctx, typel);
    return typel;
  }

  // This produced a Bad Cast but should never be called
  return 0;
}

visitd(Binrel) {
  TypesMgr::TypeId typel = visit(ctx->expression()[0]);
  TypesMgr::TypeId typer = visit(ctx->expression()[1]);

  if (!Types.isErrorTy(typel) && !Types.isErrorTy(typer) &&
      !Types.comparableTypes(typel, typer, ctx->op->getText()))
    Errors.incompatibleOperator(ctx->op);

  return Types.createBooleanTy();
}

visitd(Binbool) {
  TypesMgr::TypeId typel = visit(ctx->expression()[0]);
  TypesMgr::TypeId typer = visit(ctx->expression()[1]);

  if (!Types.isErrorTy(typel) && !Types.isErrorTy(typer) &&
      (!Types.isBooleanTy(typel) || !Types.isBooleanTy(typer)))
    Errors.incompatibleOperator(ctx->AND() ? ctx->AND()->getSymbol() : ctx->OR()->getSymbol());

  
  return Types.createBooleanTy();
}

visitd(Assignable) {
  std::string id = ctx->ID()->getText();
  
  if (Symbols.findInStack(id) == -1) {
    Errors.undeclaredIdent(ctx->ID());
    return Types.createErrorTy();
  }

  TypesMgr::TypeId idTy = Symbols.getType(id);
  
  if (ctx->expression()) {    
    TypesMgr::TypeId inTy = visit(ctx->expression());

    if (Types.isErrorTy(inTy))
      return inTy;

    if (!Types.isIntegerTy(inTy)) 
      Errors.nonIntegerIndexInArrayAccess(ctx->expression());

    if (!Types.isArrayTy(idTy)) {
      Errors.nonArrayInArrayAccess(ctx);
      return Types.createErrorTy();
    }

    TypesMgr::TypeId arrTy = Types.getArrayElemType(idTy);
    putTypeDecor(ctx, arrTy);
    return arrTy;
  }

  putTypeDecor(ctx, idTy);
  return idTy;
}

visitd(Literal) {
  TypesMgr::TypeId ty;

  if (ctx->INTLIT())
    ty = Types.createIntegerTy();
  if (ctx->FLOATLIT())
    ty = Types.createFloatTy();
  if (ctx->BOOLLIT())
    ty = Types.createBooleanTy();
  if (ctx->CHARLIT())
    ty = Types.createCharacterTy();

  putTypeDecor(ctx, ty);
  return ty;
}


// antlrcpp::Any TypeCheckVisitor::visitDeclarations(AslParser::DeclarationsContext *ctx) {
//   DEBUG_ENTER();
//   antlrcpp::Any r = visitChildren(ctx);
//   DEBUG_EXIT();
//   return r;
// }

// antlrcpp::Any TypeCheckVisitor::visitVariable_decl(AslParser::Variable_declContext *ctx) {
//   DEBUG_ENTER();
//   antlrcpp::Any r = visitChildren(ctx);
//   DEBUG_EXIT();
 //   return r;
// }

// antlrcpp::Any TypeCheckVisitor::visitType(AslParser::TypeContext *ctx) {
//   DEBUG_ENTER();
//   antlrcpp::Any r = visitChildren(ctx);
//   DEBUG_EXIT();
//   return r;
// }
/*
antlrcpp::Any TypeCheckVisitor::visitStatements(AslParser::StatementsContext *ctx) {
  DEBUG_ENTER();
  visitChildren(ctx);
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any TypeCheckVisitor::visitAssignStmt(AslParser::AssignStmtContext *ctx) {
  DEBUG_ENTER();
  visit(ctx->left_expr());
  visit(ctx->expr());
  TypesMgr::TypeId t1 = getTypeDecor(ctx->left_expr());
  TypesMgr::TypeId t2 = getTypeDecor(ctx->expr());
  if ((not Types.isErrorTy(t1)) and (not Types.isErrorTy(t2)) and
      (not Types.copyableTypes(t1, t2)))
    Errors.incompatibleAssignment(ctx->ASSIGN());
  if ((not Types.isErrorTy(t1)) and (not getIsLValueDecor(ctx->left_expr())))
    Errors.nonReferenceableLeftExpr(ctx->left_expr());
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any TypeCheckVisitor::visitIfStmt(AslParser::IfStmtContext *ctx) {
  DEBUG_ENTER();
  visit(ctx->expr());
  TypesMgr::TypeId t1 = getTypeDecor(ctx->expr());
  if ((not Types.isErrorTy(t1)) and (not Types.isBooleanTy(t1)))
    Errors.booleanRequired(ctx);
  visit(ctx->statements());
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any TypeCheckVisitor::visitProcCall(AslParser::ProcCallContext *ctx) {
  DEBUG_ENTER();
  visit(ctx->ident());
  TypesMgr::TypeId t1 = getTypeDecor(ctx->ident());
  if (not Types.isFunctionTy(t1) and not Types.isErrorTy(t1)) {
    Errors.isNotCallable(ctx->ident());
  }
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any TypeCheckVisitor::visitReadStmt(AslParser::ReadStmtContext *ctx) {
  DEBUG_ENTER();
  visit(ctx->left_expr());
  TypesMgr::TypeId t1 = getTypeDecor(ctx->left_expr());
  if ((not Types.isErrorTy(t1)) and (not Types.isPrimitiveTy(t1)) and
      (not Types.isFunctionTy(t1)))
    Errors.readWriteRequireBasic(ctx);
  if ((not Types.isErrorTy(t1)) and (not getIsLValueDecor(ctx->left_expr())))
    Errors.nonReferenceableExpression(ctx);
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any TypeCheckVisitor::visitWriteExpr(AslParser::WriteExprContext *ctx) {
  DEBUG_ENTER();
  visit(ctx->expr());
  TypesMgr::TypeId t1 = getTypeDecor(ctx->expr());
  if ((not Types.isErrorTy(t1)) and (not Types.isPrimitiveTy(t1)))
    Errors.readWriteRequireBasic(ctx);
  DEBUG_EXIT();
  return 0;
}

// antlrcpp::Any TypeCheckVisitor::visitWriteString(AslParser::WriteStringContext *ctx) {
//   DEBUG_ENTER();
//   antlrcpp::Any r = visitChildren(ctx);
//   DEBUG_EXIT();
//   return r;
// }

antlrcpp::Any TypeCheckVisitor::visitLeft_expr(AslParser::Left_exprContext *ctx) {
  DEBUG_ENTER();
  visit(ctx->ident());
  TypesMgr::TypeId t1 = getTypeDecor(ctx->ident());
  putTypeDecor(ctx, t1);
  bool b = getIsLValueDecor(ctx->ident());
  putIsLValueDecor(ctx, b);
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any TypeCheckVisitor::visitArithmetic(AslParser::ArithmeticContext *ctx) {
  DEBUG_ENTER();
  visit(ctx->expr(0));
  TypesMgr::TypeId t1 = getTypeDecor(ctx->expr(0));
  visit(ctx->expr(1));
  TypesMgr::TypeId t2 = getTypeDecor(ctx->expr(1));
  if (((not Types.isErrorTy(t1)) and (not Types.isNumericTy(t1))) or
      ((not Types.isErrorTy(t2)) and (not Types.isNumericTy(t2))))
    Errors.incompatibleOperator(ctx->op);
  TypesMgr::TypeId t = Types.createIntegerTy();
  putTypeDecor(ctx, t);
  putIsLValueDecor(ctx, false);
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any TypeCheckVisitor::visitRelational(AslParser::RelationalContext *ctx) {
  DEBUG_ENTER();
  visit(ctx->expr(0));
  TypesMgr::TypeId t1 = getTypeDecor(ctx->expr(0));
  visit(ctx->expr(1));
  TypesMgr::TypeId t2 = getTypeDecor(ctx->expr(1));
  std::string oper = ctx->op->getText();
  if ((not Types.isErrorTy(t1)) and (not Types.isErrorTy(t2)) and
      (not Types.comparableTypes(t1, t2, oper)))
    Errors.incompatibleOperator(ctx->op);
  TypesMgr::TypeId t = Types.createBooleanTy();
  putTypeDecor(ctx, t);
  putIsLValueDecor(ctx, false);
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any TypeCheckVisitor::visitValue(AslParser::ValueContext *ctx) {
  DEBUG_ENTER();
  TypesMgr::TypeId t = Types.createIntegerTy();
  putTypeDecor(ctx, t);
  putIsLValueDecor(ctx, false);
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any T  ypeCheckVisitor::visitExprIdent(AslParser::ExprIdentContext *ctx) {
  DEBUG_ENTER();
  visit(ctx->ident());
  TypesMgr::TypeId t1 = getTypeDecor(ctx->ident());
  putTypeDecor(ctx, t1);
  bool b = getIsLValueDecor(ctx->ident());
  putIsLValueDecor(ctx, b);
  DEBUG_EXIT();
  return 0;
}

antlrcpp::Any TypeCheckVisitor::visitIdent(AslParser::IdentContext *ctx) {
  DEBUG_ENTER();
  std::string ident = ctx->getText();
  if (Symbols.findInStack(ident) == -1) {
    Errors.undeclaredIdent(ctx->ID());
    TypesMgr::TypeId te = Types.createErrorTy();
    putTypeDecor(ctx, te);
    putIsLValueDecor(ctx, true);
  }
  else {
    TypesMgr::TypeId t1 = Symbols.getType(ident);
    putTypeDecor(ctx, t1);
    if (Symbols.isFunctionClass(ident))
      putIsLValueDecor(ctx, false);
    else
      putIsLValueDecor(ctx, true);
  }
  DEBUG_EXIT();
  return 0;
}
*/

#undef visitd
#undef c

// Getters for the necessary tree node atributes:
//   Scope, Type ans IsLValue
SymTable::ScopeId TypeCheckVisitor::getScopeDecor(antlr4::ParserRuleContext *ctx) {
  return Decorations.getScope(ctx);
}
TypesMgr::TypeId TypeCheckVisitor::getTypeDecor(antlr4::ParserRuleContext *ctx) {
  return Decorations.getType(ctx);
}
bool TypeCheckVisitor::getIsLValueDecor(antlr4::ParserRuleContext *ctx) {
  return Decorations.getIsLValue(ctx);
}

// Setters for the necessary tree node attributes:
//   Scope, Type ans IsLValue
void TypeCheckVisitor::putScopeDecor(antlr4::ParserRuleContext *ctx, SymTable::ScopeId s) {
  Decorations.putScope(ctx, s);
}
void TypeCheckVisitor::putTypeDecor(antlr4::ParserRuleContext *ctx, TypesMgr::TypeId t) {
  Decorations.putType(ctx, t);
}
void TypeCheckVisitor::putIsLValueDecor(antlr4::ParserRuleContext *ctx, bool b) {
  Decorations.putIsLValue(ctx, b);
}
