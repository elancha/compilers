//////////////////////////////////////////////////////////////////////
//
//    CodeGenVisitor - Walk the parser tree to do
//                     the generation of code
//
//    Copyright (C) 2019  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.  SPAIN
//
//////////////////////////////////////////////////////////////////////

#include "CodeGenVisitor.h"

#include "antlr4-runtime.h"

#include "../common/TypesMgr.h"
#include "../common/SymTable.h"
#include "../common/TreeDecoration.h"
#include "../common/code.h"

#include <string>
#include <cstddef>    // std::size_t

// uncomment the following line to enable debugging messages with DEBUG*
// #define DEBUG_BUILD
#include "../common/debug.h"

// using namespace std;


// Constructor
CodeGenVisitor::CodeGenVisitor(TypesMgr       & Types,
                               SymTable       & Symbols,
                               TreeDecoration & Decorations) :
  Types{Types},
  Symbols{Symbols},
  Decorations{Decorations} {
}

#define t(n) AslParser::n##Context
#ifdef DEBUG_BUILD
#define o(n) antlrcpp::Any CodeGenVisitor::visit##n(t(n) *ctx) { \
               DEBUG_ENTER(); \
               auto r = n(ctx); \
               DEBUG_EXIT(); \
               return r; \
             } \
             antlrcpp::Any CodeGenVisitor::n(t(n) *ctx)
#else
#define o(n) antlrcpp::Any CodeGenVisitor::visit##n(t(n) *ctx)
#endif

o(Program) {
  code c;
  
  SymTable::ScopeId sc = getScopeDecor(ctx);
  Symbols.pushThisScope(sc);
  
  for (t(Function)* fnc : ctx->function()) {
    subroutine s = visit(fnc);
    c.add_subroutine(s);
  }

  Symbols.popScope();

  return c; 
}

o(Function) {
  subroutine s(ctx->ID()->getText());

  SymTable::ScopeId sc = getScopeDecor(ctx);
  Symbols.pushThisScope(sc);

  std::string retpar = "_result";
  std::vector<std::string> args;

  if (ctx->arglist()) {
    std::vector<std::string> && rargs = visit(ctx->arglist());
    args = rargs;
    for (std::string const& ar : args)
      retpar += ar;
  }


  if (!Types.isVoidTy(Types.getFuncReturnType(getTypeDecor(ctx)))) {
    s.add_param(retpar);
    retparameter = retpar;
  }

  for (std::string arg : args)
    s.add_param(arg);

  for (t(Declaration)* d : ctx->declaration()) {
    std::pair<std::size_t, std::vector<std::string>> && vars = visit(d);
    for (std::string va : vars.second)
      s.add_var(va, vars.first);
  }

  instructionList && code = visit(ctx->statements());

  code = code || instruction::RETURN();
  s.set_instructions(code);

  Symbols.popScope();

  return s;
} 

o(Arglist) {
  std::vector<std::string> ret;
  for (t(Arg) *arg : ctx->arg())
    ret.push_back(arg->ID()->getText());
  return ret;
}

o(Declaration) {
  std::vector<antlr4::tree::TerminalNode*> a = ctx->ID();
  std::vector<std::string> result(a.size());
  std::transform(a.begin(), a.end(), result.begin(), [](antlr4::tree::TerminalNode* a) {return a->getText();});

  std::size_t s = Types.getSizeOfType(getTypeDecor(ctx));
  return make_pair(s, result);
}

o(Statements) {
  instructionList r;
  for (t(Statement)* st : ctx->statement()) {
    instructionList && ins = visit(st);
    r = r || ins;
  }
  return r;
}

// #############
// # STATEMENT #
// #############

o(If) {
  std::string endif = "endif" + codeCounters.newLabelIF();
  
  CodeAttribs atr = visit(ctx->expression());
  instructionList r = atr.code;
  r = r || instruction::FJUMP(atr.addr, endif);
  
  instructionList i = visit(ctx->statements()[0]);
  r = r || i;

  if (ctx->ELSE()) {
    std::string real_endif = "endif" + codeCounters.newLabelIF();
    instructionList e = visit(ctx->statements()[1]);
    r = r || instruction::UJUMP(real_endif) || instruction::LABEL(endif) || e;

    endif = real_endif;
  }

  r = r || instruction::LABEL(endif);
  return r;
}

o(While) {
  std::string begwhile = "while" + codeCounters.newLabelWHILE();
  std::string endwhile = "endwhile" + codeCounters.newLabelWHILE();
  
  CodeAttribs atr = visit(ctx->expression());
  instructionList body = visit(ctx->statements());
  
  return instruction::LABEL(begwhile) || atr.code || instruction::FJUMP(atr.addr, endwhile) ||
           body ||
           instruction:: UJUMP(begwhile) ||
         instruction::LABEL(endwhile);
}

o(Return) {
  instructionList r;
  if (ctx->expression()) {
    CodeAttribs atr = visit(ctx->expression());
    r = atr.code || instruction::LOAD(retparameter, atr.addr);
  }
  return r || instruction::RETURN();
}

o(Read) {
  TypesMgr::TypeId ty = getTypeDecor(ctx);
  CodeAttribs atr = visit(ctx->assignable());

  instructionList ins = atr.code;
  std::string readto = atr.addr;

  if (atr.offs != "")
    readto = '%' + codeCounters.newTEMP();

  if (Types.isIntegerTy(ty)) {
    ins = ins || instruction::READI(readto);
  } else if (Types.isFloatTy(ty)) {
    ins = ins || instruction::READF(readto);
  } else if (Types.isCharacterTy(ty)) {
    ins = ins || instruction::READC(readto);
  } else if (Types.isBooleanTy(ty)) {
    ins = ins || instruction::READI(readto);
  }

  if (atr.offs == "-")
      ins = ins || instruction::CLOAD(atr.addr, readto);
  else if (atr.offs != "")
      ins = ins || instruction::XLOAD(atr.addr, atr.offs, readto);

  return ins;
}

o(Write) {
  if (ctx->STRING()) {
    instructionList ins;
    std::string str = ctx->STRING()->getText();
    std::string temp = '%' + codeCounters.newTEMP();
    bool bs = false;
    for (unsigned int i = 1; i < str.size()-1; i++) {
        if (bs) {
          ins = ins || instruction::CHLOAD(temp, "\\" + std::string(1, str[i]));
          bs = false;
        } else {
          if (str[i] == '\\') {
            bs = true;
            continue;
          }
          ins = ins || instruction::CHLOAD(temp, std::string(1, str[i]));
        }
        ins = ins || instruction::WRITEC(temp);
    }
    return ins;
  } else {
    TypesMgr::TypeId ty = getTypeDecor(ctx);
    CodeAttribs atr = visit(ctx->expression());
    
    if (Types.isIntegerTy(ty)) {
      return atr.code || instruction::WRITEI(atr.addr);
    } else if (Types.isFloatTy(ty)) {
      return atr.code || instruction::WRITEF(atr.addr);
    } else if (Types.isCharacterTy(ty)) {
      return atr.code || instruction::WRITEC(atr.addr);
    } else if (Types.isBooleanTy(ty)) {
      return atr.code || instruction::WRITEI(atr.addr);
    }
  }

  // Should never be reached, so return null that hopefully throws an error somewhere
  return 0;
}

o(Assign) {
  CodeAttribs ass = visit(ctx->assignable());
  CodeAttribs exp = visit(ctx->expression());

  TypesMgr::TypeId arrTy = getTypeDecor(ctx->assignable());
  if (Types.isArrayTy(arrTy)) {
    instructionList ins = ass.code || exp.code;
    std::string larr = ctx->assignable()->getText();
    std::string rarr = ctx->expression()->getText();

    std::string tmpl = '%' + codeCounters.newTEMP();
    std::string tmpr = '%' + codeCounters.newTEMP();
    std::string tmp = '%' + codeCounters.newTEMP();
    std::string one = '%' + codeCounters.newTEMP();

    ins = ins || instruction::ILOAD(one, "1");

    if (Symbols.isParameterClass(larr))
      ins = ins || instruction::LOAD(tmpl, larr);
    else
      ins = ins || instruction::ALOAD(tmpl, larr);

    if (Symbols.isParameterClass(rarr))
      ins = ins || instruction::LOAD(tmpr, rarr);
    else
      ins = ins || instruction::ALOAD(tmpr, rarr);

    for (unsigned int i = 0; i < Types.getArraySize(arrTy); i++) {
      ins = ins ||
          instruction::LOADC(tmp, tmpr) ||
          instruction::CLOAD(tmpl, tmp) ||
          instruction::ADD(tmpl, tmpl, one) ||
          instruction::ADD(tmpr, tmpr, one);
    }
    return ins;
  }

  if (ass.offs == "")
    return ass.code || exp.code || instruction::LOAD(ass.addr, exp.addr);
  else if (ass.offs == "-")
    return ass.code || exp.code || instruction::CLOAD(ass.addr, exp.addr);
  else
    return ass.code || exp.code || instruction::XLOAD(ass.addr, ass.offs, exp.addr);
}

o(Expr) {
  CodeAttribs atr = visit(ctx->expression());
  return atr.code;
}


// ##############
// # EXPRESSION #
// ##############

o(Lit) {
  return visit(ctx->literal());
}

o(Ident) {
  instructionList ins;
  std::string ret = ctx->ID()->getText();

  return CodeAttribs(ret, "", ins);
}

o(Parexpr) {
  return visit(ctx->expression()); 
}

o(Fcall) {
  instructionList ins;

  if (!Types.isVoidTy(getTypeDecor(ctx)))
    ins = ins || instruction::PUSH("");

  TypesMgr::TypeId funcTy = Symbols.getType(ctx->ID()->getText());
  std::vector<TypesMgr::TypeId> parsTy = Types.getFuncParamsTypes(funcTy);

  std::vector<t(Expression)*> expr = ctx->expression();
  for (unsigned int i = 0; i < expr.size(); i++) {
    CodeAttribs p = visit(expr[i]);

    ins = ins || p.code;
    TypesMgr::TypeId expTy = getTypeDecor(expr[i]);
    if (Types.isArrayTy(expTy)) {
      std::string tmp = '%' + codeCounters.newTEMP();
      ins = ins || p.code || instruction::ALOAD(tmp, p.addr);
      p.addr = tmp;
    }
    if (Types.isIntegerTy(expTy) and Types.isFloatTy(parsTy[i])) {
      std::string tmp = '%' + codeCounters.newTEMP();
      ins = ins || instruction::FLOAT(tmp, p.addr);
      p.addr = tmp;
    }
    ins = ins || instruction::PUSH(p.addr);
  }

  ins = ins || instruction::CALL(ctx->ID()->getText());

  for (unsigned int i = 0; i < expr.size(); i++) {
    ins = ins || instruction::POP("");
  }

  std::string ret = '%' + codeCounters.newTEMP();
  if (!Types.isVoidTy(getTypeDecor(ctx)))
    ins = ins || instruction::POP(ret);

  return CodeAttribs(ret, "", ins);
}

o(Arrayindex) {
  std::string arr = ctx->ID()->getText();
  CodeAttribs idx = visit(ctx->expression());
  std::string ret = '%' + codeCounters.newTEMP();
  if (Symbols.isParameterClass(arr)) {
    std::string tmp = '%' + codeCounters.newTEMP();
    return CodeAttribs(ret, "-", idx.code ||
            instruction::ADD(tmp, idx.addr, arr) ||
            instruction::LOADC(ret, tmp));
  } else {
    return CodeAttribs(ret, "", idx.code || instruction::LOADX(ret, arr, idx.addr));
  }
}

o(Unop) {
  CodeAttribs exp = visit(ctx->expression());
  std::string ret = '%' + codeCounters.newTEMP();
  auto op = ctx->op->getType();
  
  if (op == AslParser::PLUS) {
    return exp;
  } else if (op == AslParser::NOT) {
    return CodeAttribs(ret, "", exp.code || instruction::NOT(ret, exp.addr));
  } else if (op == AslParser::MINUS){
      TypesMgr::TypeId ty = getTypeDecor(ctx);

      if (Types.isIntegerTy(ty))
        return CodeAttribs(ret, "", exp.code || instruction::NEG(ret, exp.addr));
      else if (Types.isFloatTy(ty))
        return CodeAttribs(ret, "", exp.code || instruction::FNEG(ret, exp.addr));
  }

  return 0;
}

o(Binop) {
  CodeAttribs e1 = visit(ctx->expression()[0]);
  TypesMgr::TypeId ty1 = getTypeDecor(ctx->expression()[0]);
  CodeAttribs e2 = visit(ctx->expression()[1]);
  TypesMgr::TypeId ty2 = getTypeDecor(ctx->expression()[1]);

  std::string ret = '%' + codeCounters.newTEMP();
  auto op = ctx->op->getType();

  instructionList ins = e1.code || e2.code;

  bool fl = Types.isFloatTy(ty1) or Types.isFloatTy(ty2);
  // Cast
  if (fl) {
    std::string tmp = '%' + codeCounters.newTEMP();
    if (Types.isIntegerTy(ty1)) {
        ins = ins || instruction::FLOAT(tmp, e1.addr);
        e1.addr = tmp;
    }
    if (Types.isIntegerTy(ty2)) {
        ins = ins || instruction::FLOAT(tmp, e2.addr);
        e2.addr = tmp;
    }
  }

#define lambda(OP, IST) if (op == AslParser::OP) { \
    if (fl) { ins = ins || instruction::F##IST(ret, e1.addr, e2.addr); } \
    else { ins = ins || instruction::IST(ret, e1.addr, e2.addr); }}

  lambda(MUL, MUL);
  lambda(DIV, DIV);
  lambda(PLUS, ADD);
  lambda(MINUS, SUB);

#undef lambda
  
  if (op == AslParser::MOD) {
    std::string tmp1 = '%' + codeCounters.newTEMP();
    std::string tmp2 = '%' + codeCounters.newTEMP();
    ins = ins ||
        instruction::DIV(tmp1, e1.addr, e2.addr) ||
        instruction::MUL(tmp2, e2.addr, tmp1) ||
        instruction::SUB(ret, e1.addr, tmp2);
  }

  return CodeAttribs(ret, "", ins);
}

o(Binrel) {
  CodeAttribs e1 = visit(ctx->expression()[0]);
  TypesMgr::TypeId ty1 = getTypeDecor(ctx->expression()[0]);
  CodeAttribs e2 = visit(ctx->expression()[1]);
  TypesMgr::TypeId ty2 = getTypeDecor(ctx->expression()[1]);
  std::string ret = '%' + codeCounters.newTEMP();
  auto op = ctx->op->getType();

  instructionList ins = e1.code || e2.code;

  bool fl = Types.isFloatTy(ty1) or Types.isFloatTy(ty2);
  // Cast
  if (fl) {
    std::string tmp = '%' + codeCounters.newTEMP();
    if (Types.isIntegerTy(ty1)) {
        ins = ins || instruction::FLOAT(tmp, e1.addr);
        e1.addr = tmp;
    }
    if (Types.isIntegerTy(ty2)) {
        ins = ins || instruction::FLOAT(tmp, e2.addr);
        e2.addr = tmp;
    }
  }

#define lambda(OP, IST) if (op == AslParser::OP) { \
    if (fl) { ins = ins || instruction::F##IST(ret, e1.addr, e2.addr); } \
    else { ins = ins || instruction::IST(ret, e1.addr, e2.addr); }}
#define nlambda(OP, IST) if (op == AslParser::OP) { \
    std::string tmp = '%' + codeCounters.newTEMP(); \
    if (fl) { ins = ins || instruction::F##IST(tmp, e1.addr, e2.addr); } \
    else { ins = ins || instruction::IST(tmp, e1.addr, e2.addr); }\
    ins = ins || instruction::NOT(ret, tmp); }

  lambda(EQUAL, EQ);
  nlambda(NEQUAL, EQ);
  nlambda(GT, LE);
  nlambda(GTE, LT);
  lambda(LT, LT);
  lambda(LTE, LE);

#undef lambda
#undef nlambda

  return CodeAttribs(ret, "", ins);
}

o(Binbool) {
  CodeAttribs e1 = visit(ctx->expression()[0]);
  CodeAttribs e2 = visit(ctx->expression()[1]);
  std::string ret = '%' + codeCounters.newTEMP();

  if (ctx->AND()) {
    return CodeAttribs(ret, "", e1.code || e2.code || instruction::AND(ret, e1.addr, e2.addr));
  } else {
    return CodeAttribs(ret, "", e1.code || e2.code || instruction::OR(ret, e1.addr, e2.addr));
  }
}

o(Assignable) {
  instructionList ins;
  std::string ret = ctx->ID()->getText();
  std::string off = "";

  if (ctx->expression()) {
    CodeAttribs e = visit(ctx->expression());
    ins = ins || e.code;
    if (Symbols.isParameterClass(ret)) {
        off = "-";
        std::string tmp = '%' + codeCounters.newTEMP();
        ins = ins || instruction::ADD(tmp, e.addr, ret);
        ret = tmp;
    } else {
        off = e.addr;
    }
  }
  
  return CodeAttribs(ret, off, ins);
}

o(Literal) {
  std::string tmp = '%' + codeCounters.newTEMP();
  std::string lit;
  TypesMgr::TypeId ty = getTypeDecor(ctx);

  if (ctx->INTLIT()) {
    lit = ctx->INTLIT()->getText();
  } else if (ctx->FLOATLIT()) {
    lit = ctx->FLOATLIT()->getText();
  } else if (ctx->BOOLLIT()) {
    lit = ctx->BOOLLIT()->getText() == "false" ? "0" : "1";
  } else if (ctx->CHARLIT()) {
    lit = ctx->CHARLIT()->getText();
    lit = lit.substr(1, lit.size()-2);
  }

  if (Types.isIntegerTy(ty))
    return CodeAttribs(tmp, "", instruction::ILOAD(tmp, lit));
  if (Types.isFloatTy(ty))
    return CodeAttribs(tmp, "", instruction::FLOAD(tmp, lit));
  if (Types.isBooleanTy(ty))
    return CodeAttribs(tmp, "", instruction::ILOAD(tmp, lit));
  if (Types.isCharacterTy(ty))
    return CodeAttribs(tmp, "", instruction::CHLOAD(tmp, lit));

  // Should never be reached, so return null that hopefully throws an error somewhere
  return 0;
}

#undef o
#undef t

// Methods to visit each kind of node:
/*
antlrcpp::Any CodeGenVisitor::visitProgram(AslParser::ProgramContext *ctx) {
  DEBUG_ENTER();
  code my_code;
  SymTable::ScopeId sc = getScopeDecor(ctx);
  Symbols.pushThisScope(sc);
  for (auto ctxFunc : ctx->function()) { 
    subroutine subr = visit(ctxFunc);
    my_code.add_subroutine(subr);
  }
  Symbols.popScope();
  DEBUG_EXIT();
  return my_code;
}

antlrcpp::Any CodeGenVisitor::visitFunction(AslParser::FunctionContext *ctx) {
  DEBUG_ENTER();
  SymTable::ScopeId sc = getScopeDecor(ctx);
  Symbols.pushThisScope(sc);
  subroutine subr(ctx->ID()->getText());
  codeCounters.reset();
  std::vector<var> && lvars = visit(ctx->declarations());
  for (auto & onevar : lvars) {
    subr.add_var(onevar);
  }
  instructionList && code = visit(ctx->statements());
  code = code || instruction(instruction::RETURN());
  subr.set_instructions(code);
  Symbols.popScope();
  DEBUG_EXIT();
  return subr;
}

antlrcpp::Any CodeGenVisitor::visitDeclarations(AslParser::DeclarationsContext *ctx) {
  DEBUG_ENTER();
  std::vector<var> lvars;
  for (auto & varDeclCtx : ctx->variable_decl()) {
    var onevar = visit(varDeclCtx);
    lvars.push_back(onevar);
  }
  DEBUG_EXIT();
  return lvars;
}

antlrcpp::Any CodeGenVisitor::visitVariable_decl(AslParser::Variable_declContext *ctx) {
  DEBUG_ENTER();
  TypesMgr::TypeId   t1 = getTypeDecor(ctx->type());
  std::size_t      size = Types.getSizeOfType(t1);
  DEBUG_EXIT();
  return var{ctx->ID()->getText(), size};
}

antlrcpp::Any CodeGenVisitor::visitStatements(AslParser::StatementsContext *ctx) {
  DEBUG_ENTER();
  instructionList code;
  for (auto stCtx : ctx->statement()) {
    instructionList && codeS = visit(stCtx);
    code = code || codeS;
  }
  DEBUG_EXIT();
  return code;
}

antlrcpp::Any CodeGenVisitor::visitAssignStmt(AslParser::AssignStmtContext *ctx) {
  DEBUG_ENTER();
  instructionList code;
  CodeAttribs     && codAtsE1 = visit(ctx->left_expr());
  std::string           addr1 = codAtsE1.addr;
  // std::string           offs1 = codAtsE1.offs;
  instructionList &     code1 = codAtsE1.code;
  // TypesMgr::TypeId tid1 = getTypeDecor(ctx->left_expr());
  CodeAttribs     && codAtsE2 = visit(ctx->expr());
  std::string           addr2 = codAtsE2.addr;
  // std::string           offs2 = codAtsE2.offs;
  instructionList &     code2 = codAtsE2.code;
  // TypesMgr::TypeId tid2 = getTypeDecor(ctx->expr());
  code = code1 || code2 || instruction::LOAD(addr1, addr2);
  DEBUG_EXIT();
  return code;
}

antlrcpp::Any CodeGenVisitor::visitIfStmt(AslParser::IfStmtContext *ctx) {
  DEBUG_ENTER();
  instructionList code;
  CodeAttribs     && codAtsE = visit(ctx->expr());
  std::string          addr1 = codAtsE.addr;
  instructionList &    code1 = codAtsE.code;
  instructionList &&   code2 = visit(ctx->statements());
  std::string label = codeCounters.newLabelIF();
  std::string labelEndIf = "endif"+label;
  code = code1 || instruction::FJUMP(addr1, labelEndIf) ||
         code2 || instruction::LABEL(labelEndIf);
  DEBUG_EXIT();
  return code;
}

antlrcpp::Any CodeGenVisitor::visitProcCall(AslParser::ProcCallContext *ctx) {
  DEBUG_ENTER();
  instructionList code;
  // std::string name = ctx->ident()->ID()->getSymbol()->getText();
  std::string name = ctx->ident()->getText();
  code = instruction::CALL(name);
  DEBUG_EXIT();
  return code;
}

antlrcpp::Any CodeGenVisitor::visitReadStmt(AslParser::ReadStmtContext *ctx) {
  DEBUG_ENTER();
  CodeAttribs     && codAtsE = visit(ctx->left_expr());
  std::string          addr1 = codAtsE.addr;
  // std::string          offs1 = codAtsE.offs;
  instructionList &    code1 = codAtsE.code;
  instructionList &     code = code1;
  // TypesMgr::TypeId tid1 = getTypeDecor(ctx->left_expr());
  code = code1 || instruction::READI(addr1);
  DEBUG_EXIT();
  return code;
}

antlrcpp::Any CodeGenVisitor::visitWriteExpr(AslParser::WriteExprContext *ctx) {
  DEBUG_ENTER();
  CodeAttribs     && codAt1 = visit(ctx->expr());
  std::string         addr1 = codAt1.addr;
  // std::string         offs1 = codAt1.offs;
  instructionList &   code1 = codAt1.code;
  instructionList &    code = code1;
  // TypesMgr::TypeId tid1 = getTypeDecor(ctx->expr());
  code = code1 || instruction::WRITEI(addr1);
  DEBUG_EXIT();
  return code;
}

antlrcpp::Any CodeGenVisitor::visitWriteString(AslParser::WriteStringContext *ctx) {
  DEBUG_ENTER();
  instructionList code;
  std::string s = ctx->STRING()->getText();
  std::string temp = "%"+'%' + codeCounters.newTEMP();
  int i = 1;
  while (i < int(s.size())-1) {
    if (s[i] != '\\') {
      code = code ||
	     instruction::CHLOAD(temp, s.substr(i,1)) ||
	     instruction::WRITEC(temp);
      i += 1;
    }
    else {
      assert(i < int(s.size())-2);
      if (s[i+1] == 'n') {
        code = code || instruction::WRITELN();
        i += 2;
      }
      else if (s[i+1] == 't' or s[i+1] == '"' or s[i+1] == '\\') {
        code = code ||
               instruction::CHLOAD(temp, s.substr(i,2)) ||
	       instruction::WRITEC(temp);
        i += 2;
      }
      else {
        code = code ||
               instruction::CHLOAD(temp, s.substr(i,1)) ||
	       instruction::WRITEC(temp);
        i += 1;
      }
    }
  }
  DEBUG_EXIT();
  return code;
}

antlrcpp::Any CodeGenVisitor::visitLeft_expr(AslParser::Left_exprContext *ctx) {
  DEBUG_ENTER();
  CodeAttribs && codAts = visit(ctx->ident());
  DEBUG_EXIT();
  return codAts;
}

antlrcpp::Any CodeGenVisitor::visitArithmetic(AslParser::ArithmeticContext *ctx) {
  DEBUG_ENTER();
  CodeAttribs     && codAt1 = visit(ctx->expr(0));
  std::string         addr1 = codAt1.addr;
  instructionList &   code1 = codAt1.code;
  CodeAttribs     && codAt2 = visit(ctx->expr(1));
  std::string         addr2 = codAt2.addr;
  instructionList &   code2 = codAt2.code;
  instructionList &&   code = code1 || code2;
  // TypesMgr::TypeId t1 = getTypeDecor(ctx->expr(0));
  // TypesMgr::TypeId t2 = getTypeDecor(ctx->expr(1));
  // TypesMgr::TypeId  t = getTypeDecor(ctx);
  std::string temp = "%"+'%' + codeCounters.newTEMP();
  if (ctx->MUL())
    code = code || instruction::MUL(temp, addr1, addr2);
  else // (ctx->PLUS())
    code = code || instruction::ADD(temp, addr1, addr2);
  CodeAttribs codAts(temp, "", code);
  DEBUG_EXIT();
  return codAts;
}

antlrcpp::Any CodeGenVisitor::visitRelational(AslParser::RelationalContext *ctx) {
  DEBUG_ENTER();
  CodeAttribs     && codAt1 = visit(ctx->expr(0));
  std::string         addr1 = codAt1.addr;
  instructionList &   code1 = codAt1.code;
  CodeAttribs     && codAt2 = visit(ctx->expr(1));
  std::string         addr2 = codAt2.addr;
  instructionList &   code2 = codAt2.code;
  instructionList &&   code = code1 || code2;
  // TypesMgr::TypeId t1 = getTypeDecor(ctx->expr(0));
  // TypesMgr::TypeId t2 = getTypeDecor(ctx->expr(1));
  // TypesMgr::TypeId  t = getTypeDecor(ctx);
  std::string temp = "%"+'%' + codeCounters.newTEMP();
  code = code || instruction::EQ(temp, addr1, addr2);
  CodeAttribs codAts(temp, "", code);
  DEBUG_EXIT();
  return codAts;
}

antlrcpp::Any CodeGenVisitor::visitValue(AslParser::ValueContext *ctx) {
  DEBUG_ENTER();
  instructionList code;
  std::string temp = "%"+'%' + codeCounters.newTEMP();
  code = instruction::ILOAD(temp, ctx->getText());
  CodeAttribs codAts(temp, "", code);
  DEBUG_EXIT();
  return codAts;
}

antlrcpp::Any CodeGenVisitor::visitExprIdent(AslParser::ExprIdentContext *ctx) {
  DEBUG_ENTER();
  CodeAttribs && codAts = visit(ctx->ident());
  DEBUG_EXIT();
  return codAts;
}

antlrcpp::Any CodeGenVisitor::visitIdent(AslParser::IdentContext *ctx) {
  DEBUG_ENTER();
  CodeAttribs codAts(ctx->ID()->getText(), "", instructionList());
  DEBUG_EXIT();
  return codAts;
}
*/

// Getters for the necessary tree node atributes:
//   Scope and Type
SymTable::ScopeId CodeGenVisitor::getScopeDecor(antlr4::ParserRuleContext *ctx) const {
  return Decorations.getScope(ctx);
}
TypesMgr::TypeId CodeGenVisitor::getTypeDecor(antlr4::ParserRuleContext *ctx) const {
  return Decorations.getType(ctx);
}


// Constructors of the class CodeAttribs:
//
CodeGenVisitor::CodeAttribs::CodeAttribs(const std::string & addr,
					 const std::string & offs,
					 instructionList & code) :
  addr{addr}, offs{offs}, code{code} {
}

CodeGenVisitor::CodeAttribs::CodeAttribs(const std::string & addr,
					 const std::string & offs,
					 instructionList && code) :
  addr{addr}, offs{offs}, code{code} {
}
