//////////////////////////////////////////////////////////////////////
//
//    Asl - Another simple language (grammar)
//
//    Copyright (C) 2017  Universitat Politecnica de Catalunya
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU General Public License
//    as published by the Free Software Foundation; either version 3
//    of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
//    contact: José Miguel Rivero (rivero@cs.upc.edu)
//             Computer Science Department
//             Universitat Politecnica de Catalunya
//             despatx Omega.110 - Campus Nord UPC
//             08034 Barcelona.  SPAIN
//
//////////////////////////////////////////////////////////////////////

grammar Asl;

//////////////////////////////////////////////////
/// Parser Rules
//////////////////////////////////////////////////

program : function* EOF
        ;

function
        : FUNC ID LPAR arglist? RPAR (COLON type)? declaration* statements ENDFUNC
        ;

arglist
        : arg (COMMA arg)*
        ;

arg
        : ID COLON type
        ;
        
declaration
        : VAR ID (COMMA ID)* COLON type
        ;

statements
        : statement*
        ;

type
        : basictype
        | ARRAY LSQ INTLIT RSQ OF basictype
        ;

basictype
        : INT
	    | FLOAT
    	| BOOL
	    | CHAR
        ;

// The different types of instructions
statement
        : IF expression THEN statements (ELSE statements)? ENDIF # if
        | WHILE expression DO statements ENDWHILE                # while
        | RETURN expression? SCLN                                # return
        | READ assignable SCLN                                   # read
        | WRITE (expression | STRING) SCLN                       # write
        | assignable ASSIGN expression SCLN                      # assign
        | expression SCLN                                        # expr
        ;

expression
        : literal                                                # lit
        | ID                                                     # ident
        | LPAR expression RPAR                                   # parexpr
        | ID LPAR (expression (COMMA expression)*)? RPAR         # fcall
        // Now the order means operator precedence from highest to lowest
        | ID LSQ expression RSQ                                  # arrayindex
        // Unary expressions 
        | op=(NOT | PLUS | MINUS) expression                     # unop
        // Binary expressions
        | expression op=(MUL | DIV | MOD) expression             # binop
        | expression op=(PLUS | MINUS) expression                # binop
        | expression op=(EQUAL | NEQUAL | GT | GTE | LT | LTE) expression # binrel
        | expression AND expression                              # binbool
        | expression OR expression                               # binbool
        ; 


assignable
        : ID
        | ID LSQ expression RSQ //TODO(ernesto): ONLY IDs are valid??
        ;

literal
        : INTLIT
        | FLOATLIT
        | BOOLLIT
        | CHARLIT
        ;

//////////////////////////////////////////////////
/// Lexer Rules
//////////////////////////////////////////////////

// Unary/Binary operators
ASSIGN    : '=' ;

NOT       : 'not';

MUL       : '*';
DIV       : '/';
MOD       : '%';

PLUS      : '+';
MINUS     : '-';

EQUAL     : '==';
NEQUAL    : '!=';
GT        : '>';
GTE       : '>=';
LT        : '<';
LTE       : '<=';


AND       : 'and';
OR        : 'or';

// type keywords
INT       : 'int';
FLOAT     : 'float';
BOOL      : 'bool';
CHAR      : 'char';

ARRAY     : 'array';
OF        : 'of';

// Control keywords
IF        : 'if' ;
THEN      : 'then' ;
ELSE      : 'else' ;
ENDIF     : 'endif' ;

WHILE     : 'while';
DO        : 'do';
ENDWHILE  : 'endwhile';

FUNC      : 'func' ;
ENDFUNC   : 'endfunc' ;
READ      : 'read' ;
WRITE     : 'write' ;
RETURN    : 'return';
VAR       : 'var';

// Common tokens
COLON     : ':';
LPAR      : '(';
RPAR      : ')';
LSQ       : '[';
RSQ       : ']';
SCLN      : ';';
COMMA     : ',';



INTLIT    : ('0'..'9')+ ;
FLOATLIT  : ('0'..'9')+ '.' ('0'..'9')+ ;
BOOLLIT   : 'true'|'false';
CHARLIT   : '\'' ( ESC_SEQ | ~('\\') ) '\'';
STRING    : '"' ( ESC_SEQ | ~('\\'|'"') )* '"' ; // Strings (in quotes) with escape sequences
ESC_SEQ   : '\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\') ;
ID        : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')* ;


// Comments (inline C++-style)
COMMENT   : '//' ~('\n'|'\r')* '\r'? '\n' -> skip ;
// White spaces
WS        : (' '|'\t'|'\r'|'\n')+ -> skip ;
// Alternative description
// WS        : [ \t\r\n]+ -> skip ;
